package routes

import (
	"net/http"
	"test-project/handlers"
)

var (
	vc *handlers.VenueHandler
)

type Route struct {
	Url     string
	Method  string
	Handler http.HandlerFunc
}

func init() {
	vc = handlers.NewVenueHandler()
}

func Register() {
	var Routes = []Route{
		{"/venues", http.MethodPost, vc.Create},
		{"/venues/rating", http.MethodPut, vc.UpdateRating},
		{"/venues/by_city", http.MethodGet, vc.GetByCity},
		{"/venues/highest", http.MethodGet, vc.GetByCityWithHighestRating},
		{"/venues/menus", http.MethodGet, vc.GetMenuByVenueId},
		{"/venues/fatal", http.MethodPost, vc.FatalAction},
	}

	for _, route := range Routes {
		http.Handle(route.Url, handlers.BaseHandler(route.Handler, route.Method))
	}
}
