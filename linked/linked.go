package linked

import (
	"fmt"
)

type Node struct {
	Value byte
	Next  *Node
}

func PrintTree(head *Node) {
	for {
		fmt.Println(string(head.Value))
		if head.Next == nil {
			return
		}

		head = head.Next
	}
}

func StringToLinkedList(str string) *Node {
	var head, previous *Node

	for i := 0; i < len(str); i++ {
		child := &Node{
			Value: str[i],
		}

		if previous == nil {
			head = child
		} else {
			previous.Next = child
		}

		previous = child
	}

	return head
}
