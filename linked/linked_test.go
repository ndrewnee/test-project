package linked

import "testing"

func TestLinkedList(t *testing.T) {
	input := "Hello World!"
	head := StringToLinkedList(input)
	PrintTree(head)

	for i := 0; i < len(input); i++ {
		if input[i] != head.Value {
			t.Errorf("expected %s to be %s\n", string(head.Value), string(input[i]))
		}

		head = head.Next
	}
}
