package venue

import (
	"strconv"
	"testing"
)

func TestVenue(t *testing.T) {
	venues := []*Venue{}
	venues = append(venues, NewVenue(), NewVenue())

	for index, venue := range venues {
		indexStr := strconv.Itoa(index)

		menu := "menu-" + indexStr
		reviewer := "reviewer-" + indexStr

		venue.AddMenu(Menu{Name: menu})
		venue.AddReview(Review{Reviewer: reviewer})

		if len(venue.Menus) != 1 {
			t.Error("expected menus's length to be 1")
		}

		if len(venue.Reviews) != 1 {
			t.Error("expected reviews's length to be 1")
		}

		if venue.Menus[0].Name != menu {
			t.Errorf("expected %s name to be %s\n", venue.Menus[0].Name, menu)
		}

		if venue.Reviews[0].Reviewer != reviewer {
			t.Errorf("expected %s reviewer to be %s\n", venue.Reviews[0].Reviewer, reviewer)
		}
	}
}
