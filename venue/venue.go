package venue

type Venue struct {
	Id      int      `json:"id"`
	Name    string   `json:"name"`
	City    string   `json:"city"`
	Address string   `json:"address"`
	Rating  float64  `json:"rating"`
	Menus   []Menu   `json:"menus"`
	Reviews []Review `json:"reviews"`
}

type Menu struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Decsription string `json:"description"`
}

type Review struct {
	Id       int     `json:"id"`
	Reviewer string  `json:"reviewer"`
	Comment  string  `json:"comment"`
	Rating   float64 `json:"rating"`
}

func NewVenue() *Venue {
	venue := &Venue{}
	venue.Menus = []Menu{}
	venue.Reviews = []Review{}

	return venue
}

func (v *Venue) AddMenu(menu Menu) {
	v.Menus = append(v.Menus, menu)
}

func (v *Venue) AddReview(review Review) {
	v.Reviews = append(v.Reviews, review)
}
