package handlers

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestBaseHandler(t *testing.T) {
	vh := NewVenueHandler()

	asserts := []struct {
		handler        http.HandlerFunc
		method         string
		expectedMethod string
		token          string
		status         int
	}{
		{vh.Create, http.MethodPost, http.MethodPost, "", http.StatusUnauthorized},
		{vh.Create, http.MethodPut, http.MethodPost, "blabl", http.StatusNotFound},
		{vh.FatalAction, http.MethodPost, http.MethodPost, "blabl", http.StatusInternalServerError},
		{vh.GetByCity, http.MethodGet, http.MethodGet, "blabl", http.StatusOK},
	}

	for _, assert := range asserts {
		r, err := http.NewRequest(assert.method, "?city=Tashkent", nil)
		if err != nil {
			t.Error(err)
			return
		}

		r.Header.Set("token", assert.token)

		w := httptest.NewRecorder()

		h := BaseHandler(assert.handler, assert.expectedMethod)

		h.ServeHTTP(w, r)

		if w.Code != assert.status {
			t.Errorf("expected code %d to be %d\n", w.Code, assert.status)
		}
	}
}
