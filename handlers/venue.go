package handlers

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"test-project/store"
	"test-project/venue"
)

var ErrManualPanic = errors.New("manual fatal error")

func NewVenueHandler() *VenueHandler {
	return &VenueHandler{
		store: store.NewVenueStore(),
	}
}

type VenueHandler struct {
	store *store.VenueStore
}

func (vh *VenueHandler) Create(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	v := venue.NewVenue()

	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	vh.store.Create(*v)
	w.WriteHeader(http.StatusCreated)
}

func (vh *VenueHandler) UpdateRating(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	v := venue.NewVenue()

	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = vh.store.UpdateRating(v.Id, v.Rating)
	if err == store.ErrNotFound {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (vh *VenueHandler) GetByCity(w http.ResponseWriter, r *http.Request) {
	city := r.URL.Query().Get("city")
	if city == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("url param \"city\" not found"))
		return
	}

	limitStr := r.URL.Query().Get("limit")
	if limitStr == "" {
		limitStr = "0"
	}

	limit, err := strconv.Atoi(limitStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("url param \"limit\" must be integer"))
		return
	}

	venues := vh.store.GetByCity(city, limit)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	err = json.NewEncoder(w).Encode(venues)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (vh *VenueHandler) GetByCityWithHighestRating(w http.ResponseWriter, r *http.Request) {
	city := r.URL.Query().Get("city")
	if city == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("url param \"city\" not found"))
		return
	}

	venue, err := vh.store.GetByCityWithHighestRating(city)
	if err == store.ErrNotFound {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("venues with such city not found"))
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	err = json.NewEncoder(w).Encode(venue)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (vh *VenueHandler) GetMenuByVenueId(w http.ResponseWriter, r *http.Request) {
	idStr := r.URL.Query().Get("venue_id")
	if idStr == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("url param \"venue_id\" not found"))
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("url param \"venue_id\" must be integer"))
		return
	}

	menus, err := vh.store.GetMenuByVenueId(id)
	if err == store.ErrNotFound {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("venue with such id not found"))
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	err = json.NewEncoder(w).Encode(menus)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (vh *VenueHandler) FatalAction(w http.ResponseWriter, r *http.Request) {
	panic(ErrManualPanic)
}
