package handlers

import (
	"errors"
	"log"
	"net/http"
)

func BaseHandler(h http.Handler, method string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			var err error
			r := recover()
			if r != nil {
				switch t := r.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("Unknown error")
				}

				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}()

		if r.Method != method {
			http.Error(w, "Handler for this method not found", http.StatusNotFound)
		}

		token := r.Header.Get("token")

		log.Printf("Request. Method:%s, URI: %s, Token: %s\n", r.Method, r.RequestURI, token)

		if token == "" {
			http.Error(w, "You are not authorized", http.StatusUnauthorized)
			return
		}

		h.ServeHTTP(w, r)
	})
}
