package handlers

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"test-project/venue"
	"testing"
)

func TestVenueHandlerCreate(t *testing.T) {
	vh := NewVenueHandler()

	venueBytes, err := json.Marshal(venue.Venue{
		Name:   "Test venue",
		City:   "Tashkent",
		Rating: 2.3,
	})

	if err != nil {
		t.Error(err)
		return
	}

	asserts := []struct {
		body   io.Reader
		status int
	}{
		{nil, http.StatusBadRequest},
		{bytes.NewReader(venueBytes), http.StatusCreated},
	}

	for _, assert := range asserts {
		r, err := http.NewRequest(http.MethodPost, "", assert.body)
		if err != nil {
			t.Error(err)
			return
		}

		w := httptest.NewRecorder()

		vh.Create(w, r)
		if w.Code != assert.status {
			t.Errorf("expected code %d to be %d\n", w.Code, assert.status)
		}
	}
}

func TestVenueHandlerUpdateRating(t *testing.T) {
	vh := NewVenueHandler()

	venueBytes, err := json.Marshal(venue.Venue{
		Id:     1,
		Rating: 2.3,
	})

	if err != nil {
		t.Error(err)
		return
	}

	notFound, err := json.Marshal(venue.Venue{
		Id:     999,
		Rating: 2.3,
	})

	if err != nil {
		t.Error(err)
		return
	}

	asserts := []struct {
		body   io.Reader
		status int
	}{
		{nil, http.StatusBadRequest},
		{bytes.NewReader(notFound), http.StatusNotFound},
		{bytes.NewReader(venueBytes), http.StatusNoContent},
	}

	for _, assert := range asserts {
		r, err := http.NewRequest(http.MethodPut, "", assert.body)
		if err != nil {
			t.Error(err)
			return
		}

		w := httptest.NewRecorder()

		vh.UpdateRating(w, r)
		if w.Code != assert.status {
			t.Errorf("expected code %d to be %d\n", w.Code, assert.status)
		}
	}
}

func TestVenueHandlerGetByCity(t *testing.T) {
	vh := NewVenueHandler()

	asserts := []struct {
		url    string
		status int
	}{
		{"/venues/by_city", http.StatusBadRequest},
		{"/venues/by_city?city=Tashkent&limit=asd", http.StatusBadRequest},
		{"/venues/by_city?city=Tashkent", http.StatusOK},
		{"/venues/by_city?city=Tashkent&limit=5", http.StatusOK},
	}

	for _, assert := range asserts {
		r, err := http.NewRequest(http.MethodPut, assert.url, nil)
		if err != nil {
			t.Error(err)
			return
		}

		w := httptest.NewRecorder()

		vh.GetByCity(w, r)
		if w.Code != assert.status {
			t.Errorf("expected code %d to be %d\n", w.Code, assert.status)
		}
	}
}

func TestVenueHandlerGetByCityWithHighestRating(t *testing.T) {
	vh := NewVenueHandler()

	asserts := []struct {
		url    string
		status int
	}{
		{"/venues/highest", http.StatusBadRequest},
		{"/venues/highest?city=Blabla", http.StatusNotFound},
		{"/venues/highest?city=Tashkent", http.StatusOK},
	}

	for _, assert := range asserts {
		r, err := http.NewRequest(http.MethodPut, assert.url, nil)
		if err != nil {
			t.Error(err)
			return
		}

		w := httptest.NewRecorder()

		vh.GetByCityWithHighestRating(w, r)
		if w.Code != assert.status {
			t.Errorf("expected code %d to be %d\n", w.Code, assert.status)
		}
	}
}

func TestVenueHandlerGetMenuByVenueId(t *testing.T) {
	vh := NewVenueHandler()

	asserts := []struct {
		url    string
		status int
	}{
		{"/venues/menus", http.StatusBadRequest},
		{"/venues/menus?venue_id=sdada", http.StatusBadRequest},
		{"/venues/menus?venue_id=8989", http.StatusNotFound},
		{"/venues/menus?venue_id=1", http.StatusOK},
	}

	for _, assert := range asserts {
		r, err := http.NewRequest(http.MethodPut, assert.url, nil)
		if err != nil {
			t.Error(err)
			return
		}

		w := httptest.NewRecorder()

		vh.GetMenuByVenueId(w, r)
		if w.Code != assert.status {
			t.Errorf("expected code %d to be %d\n", w.Code, assert.status)
		}
	}
}

func TestVenueHandlerFatalAction(t *testing.T) {
	defer func() {
		err := recover().(error)
		if err != ErrManualPanic {
			t.Errorf("expected error %s to be %s\n", err, ErrManualPanic)
		}
	}()

	vh := NewVenueHandler()

	r, err := http.NewRequest(http.MethodPost, "", nil)
	if err != nil {
		t.Error(err)
		return
	}

	w := httptest.NewRecorder()

	vh.FatalAction(w, r)
}
