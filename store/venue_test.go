package store

import (
	"test-project/venue"
	"testing"
)

func TestVenueStoreCreate(t *testing.T) {
	vs := NewVenueStore()

	testData := []venue.Venue{
		{Name: "Dolce", City: "Tashkent", Rating: 2.0, Menus: []venue.Menu{{Name: "coffee"}}},
		{Name: "Kovalsky", City: "Tashkent", Rating: 3.2},
		{Name: "GQ", City: "Tashkent", Rating: 4.2},
		{Name: "Darvoza", City: "Samarkand", Rating: 1.1},
		{Name: "Buxoro", City: "Buxara", Rating: 0.8},
	}

	for i, data := range testData {
		vs.Create(data)

		if len(vs.venuesMap) != i+1 {
			t.Errorf("expected len %d to be 1\n", len(vs.venuesMap))
		}
	}
}

func TestVenueStoreUpdateRating(t *testing.T) {
	vs := NewVenueStore()

	asserts := []struct {
		id     int
		rating float64
		err    error
	}{
		{1, 2.8, nil},
		{190, 0, ErrNotFound},
	}

	for _, assert := range asserts {
		err := vs.UpdateRating(assert.id, assert.rating)
		if err != assert.err {
			t.Error(err)
			continue
		}

		if vs.venuesMap[assert.id].Rating != assert.rating {
			t.Errorf("expected rating %v to be %v\n", vs.venuesMap[assert.id].Rating, assert.rating)
		}
	}
}

func TestVenueStoreGetByCity(t *testing.T) {
	vs := NewVenueStore()

	asserts := []struct {
		city        string
		limit       int
		expectedLen int
	}{
		{"Tashkent", 0, 3},
		{"Tashkent", 1, 1},
		{"Tashkent", 2, 2},
		{"Tashkent", 5, 3},
		{"Samarkand", 0, 1},
		{"Samarkand", 10, 1},
		{"Nukus", 10, 0},
	}

	for _, assert := range asserts {
		result := vs.GetByCity(assert.city, assert.limit)
		if len(result) != assert.expectedLen {
			t.Errorf("expected len %d to be %d\n", len(result), assert.expectedLen)
			t.Logf("%#v", result)
		}
	}
}

func TestVenueStoreGetByCityWithHighestRating(t *testing.T) {
	vs := NewVenueStore()

	v, err := vs.GetByCityWithHighestRating("Tashkent")
	if err != nil {
		t.Error(err)
	}

	if v.Name != "GQ" {
		t.Errorf("expected name %s to be GQ\n", v.Name)
	}
}

func TestVenueStoreGetMenuByVenueId(t *testing.T) {
	vs := NewVenueStore()

	menus, err := vs.GetMenuByVenueId(1)
	if err != nil {
		t.Error(err)
	}

	if len(menus) == 0 {
		t.Error("len menus == 0")
		return
	}

	if menus[0].Name != "coffee" {
		t.Errorf("expected %s to be coffee\n", menus[0].Name)
	}
}
