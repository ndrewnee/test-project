package store

import (
	"errors"
	"sync"
	"test-project/venue"
)

// For creating thread safe singleton object
var once sync.Once

// Instance of singleton object
var instance *VenueStore

var ErrNotFound = errors.New("venue not found")

func NewVenueStore() *VenueStore {
	once.Do(func() {
		instance = &VenueStore{
			venuesMap: make(map[int]venue.Venue),
		}
	})

	return instance
}

type VenueStore struct {
	sync.Mutex
	venuesMap map[int]venue.Venue
}

func (vs *VenueStore) Create(v venue.Venue) {
	vs.Lock()
	defer vs.Unlock()

	count := len(vs.venuesMap) + 1
	v.Id = count
	vs.venuesMap[count] = v
}

func (vs *VenueStore) UpdateRating(id int, rating float64) error {
	vs.Lock()
	defer vs.Unlock()

	v, ok := vs.venuesMap[id]
	if !ok {
		return ErrNotFound
	}

	v.Rating = rating
	vs.venuesMap[id] = v

	return nil
}

func (vs *VenueStore) GetByCity(city string, limit int) []venue.Venue {
	vs.Lock()
	defer vs.Unlock()

	venues := []venue.Venue{}

	for _, v := range vs.venuesMap {
		if limit != 0 && len(venues) == limit {
			return venues
		}

		if v.City == city {
			venues = append(venues, v)
		}
	}

	return venues
}

func (vs *VenueStore) GetByCityWithHighestRating(city string) (venue.Venue, error) {
	var maxRating float64
	var maxId int

	vs.Lock()
	defer vs.Unlock()

	for id, v := range vs.venuesMap {
		if v.Rating >= maxRating && v.City == city {
			maxRating = v.Rating
			maxId = id
		}
	}

	v, ok := vs.venuesMap[maxId]
	if !ok {
		return v, ErrNotFound
	}

	return v, nil
}

func (vs *VenueStore) GetMenuByVenueId(id int) ([]venue.Menu, error) {
	vs.Lock()
	defer vs.Unlock()

	_, ok := vs.venuesMap[id]
	if !ok {
		return nil, ErrNotFound
	}

	return vs.venuesMap[id].Menus, nil
}
