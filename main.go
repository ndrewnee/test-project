package main

import (
	"log"
	"net/http"
	"test-project/routes"
)

const addr = ":8089"

func main() {
	routes.Register()

	log.Println("Listening on " + addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
