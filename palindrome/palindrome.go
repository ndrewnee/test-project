package palindrome

func IsPalindrome(str string) (isPalindrome bool) {
	strLen := len(str)

	if strLen == 0 {
		return false
	}

	for i := 0; i < strLen; i++ {
		if str[i] != str[strLen-i-1] {
			return false
		}
	}

	return true
}
