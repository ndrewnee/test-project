package palindrome

import (
	"testing"
)

func TestIsPalindrome(t *testing.T) {
	asserts := []struct {
		str      string
		expected bool
	}{
		{"blabla", false},
		{"blaalb", true},
		{"blalb", true},
		{"b", true},
		{"", false},
	}

	for _, assert := range asserts {
		actual := IsPalindrome(assert.str)
		if actual != assert.expected {
			t.Errorf("expected %s, got %s\n", assert.expected, actual)
		}
	}
}
